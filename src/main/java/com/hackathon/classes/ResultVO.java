package com.hackathon.classes;

/**
 * Created by hitenpratap on 28/01/17.
 */
public class ResultVO {

    private String message;
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
