package com.hackathon.classes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class MailAPI {

    public static String sendMail(MailVO mailVO) {
        String apiResponse = "";

        try {
            String mailBody = checkEmailType(mailVO);
            String sentTo = mailVO.getSentToEmail();
            HttpURLConnection con = null;
            OutputStreamWriter writer = null;
            String inputData = " {\n  \"From\": \"hello@p2pforce.com\",\n  \"To\": \"" + sentTo + "\",\n" + "  \"Bcc\": \"blank-copied@example.com\",\n" + "  \"Subject\": \"" + mailVO.getSubject() + "\",\n" + "  \"Tag\": \"Invitation\",\n" + "  \"HtmlBody\": \"" + mailBody + "\",\n" + "  \"TextBody\": \"test\",\n" + "  \"ReplyTo\": \"reply@example.com\",\n" + "  \"Headers\": [\n" + "    { \n" + "      \"Name\": \"CUSTOM-HEADER\", \n" + "      \"Value\": \"value\"\n" + "    }\n" + "  ],\n" + "  \"TrackOpens\": true\n" + "}";
            String googleVisionURL = "https://api.postmarkapp.com/email";
            URL obj = new URL(googleVisionURL);
            con = (HttpURLConnection) obj.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("X-Postmark-Server-Token", "61eded54-7fbd-4b94-aee8-2a5b4565d336");
            writer = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
            writer.write(inputData);
            writer.flush();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer responseStream = new StringBuffer();

            while (true) {
                String inputLine;
                if ((inputLine = bufferedReader.readLine()) == null) {
                    responseStream.append(inputLine);
                    apiResponse = responseStream.toString();
                    break;
                }
            }
        } catch (Exception var12) {
            var12.printStackTrace();
        }
        return apiResponse;
    }


    public static String checkEmailType(MailVO mailVO) {
        String body = "";
        if (mailVO.getEmailType() == Enums.EmailType.SIGN_UP) {
            body = "Hii " + mailVO.getSentToUserName() + "!! <br/><br/><br/>\n" + mailVO.getDescription() + "<br/><br/><br/>\nThanking You<br/>TODO<br/>";
        } else if (mailVO.getEmailType() == Enums.EmailType.TASK_ASSIGNED) {
            body = "Hii " + mailVO.getSentToUserName() + "!! <br/> <br/><br/>\n" + mailVO.getDescription() + "<br/><br/><br/>\nThanking You<br/>\n" + mailVO.getSentByUserName() + "<br/><br/>";

        } else if (mailVO.getEmailType() == Enums.EmailType.NEW_USER_ADDED_TO_PROJECT) {
            body = "Hii " + mailVO.getSentToUserName() + "!! <br/><br/><br/> " + mailVO.getDescription() + " <br/><br/><br/>\nThanking You<br/>\n" + mailVO.getSentByUserName() + "<br/>\n<br/>";

        }

        return body;
    }
}
