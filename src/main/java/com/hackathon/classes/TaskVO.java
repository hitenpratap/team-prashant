package com.hackathon.classes;


import com.hackathon.web.model.Project;
import com.hackathon.web.model.Task;

public class TaskVO
{
    private Long projectId;
    private String taskName;
    private String taskDescription;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}
