package com.hackathon.classes;


public class MailVO {

    private String sentToEmail;
    private String sentByUserName;
    private String description;
    private Enums.EmailType emailType;
    private String sentToUserName;
    private String taskPriority;
    private String subject;
    private String projectName;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSentToEmail() {
        return sentToEmail;
    }

    public void setSentToEmail(String sentToEmail) {
        this.sentToEmail = sentToEmail;
    }

    public String getSentByUserName() {
        return sentByUserName;
    }

    public void setSentByUserName(String sentByUserName) {
        this.sentByUserName = sentByUserName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Enums.EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(Enums.EmailType emailType) {
        this.emailType = emailType;
    }

    public String getSentToUserName() {
        return sentToUserName;
    }

    public void setSentToUserName(String sentToUserName) {
        this.sentToUserName = sentToUserName;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }
}
