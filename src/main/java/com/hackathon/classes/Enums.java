package com.hackathon.classes;


public class Enums {

    public static enum EmailType {
        SIGN_UP, TASK_ASSIGNED, NEW_USER_ADDED_TO_PROJECT
    }

    public enum LabelType {
        ADMIN, MEMBER
    }

    public enum Result {
        ERROR, SUCCESS
    }
}
