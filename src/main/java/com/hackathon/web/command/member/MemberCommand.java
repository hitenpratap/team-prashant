package com.hackathon.web.command.member;

import com.hackathon.web.model.Comment;
import com.hackathon.web.model.Label;
import com.hackathon.web.model.Project;
import com.hackathon.web.model.Task;

import java.util.Date;
import java.util.Set;

public class MemberCommand {
    private Long id;
    private String password;
    private String username;
    private String firstName;
    private String lastName;
    Date lastUpdated;
    Date dateCreated;
    private Set<Project> projects;
    private Set<Task> tasks;
    private Set<Comment> comments;
    private Set<Label> labels;

    public MemberCommand(Long id, String password, String username, String firstName, String lastName, Date lastUpdated, Date dateCreated, Set<Project> projects, Set<Task> tasks, Set<Comment> comments, Set<Label> labels) {
        this.id = id;
        this.password = password;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastUpdated = lastUpdated;
        this.dateCreated = dateCreated;
        this.projects = projects;
        this.tasks = tasks;
        this.comments = comments;
        this.labels = labels;
    }

    public MemberCommand() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Label> getLabels() {
        return labels;
    }

    public void setLabels(Set<Label> labels) {
        this.labels = labels;
    }
}