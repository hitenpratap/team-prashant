package com.hackathon.web.command.label;

import com.hackathon.classes.Enums;
import com.hackathon.web.model.Label;
import com.hackathon.web.model.Task;
import com.hackathon.web.model.user.Member;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class LabelCommand {

    private Long id;
    private Date lastUpdated;
    private Date dateCreated;
    private String uniqueId = UUID.randomUUID().toString();
    private String name;
    Enums.LabelType type = Enums.LabelType.MEMBER;
    private Member member;

    private Set<Task> tasks;

    public LabelCommand(Long id, String uniqueId, String name, Enums.LabelType type, Member member) {
        this.id = id;
        this.uniqueId = uniqueId;
        this.name = name;
        this.type = type;
        this.member = member;
    }

    public LabelCommand() {

    }

    public LabelCommand(Label label) {
        this.id = label.getId();
        this.name = label.getName();
        this.type = label.getType();
        this.member = label.getMember();
        this.uniqueId = label.getUniqueId();
    }

    public LabelCommand(Long id, Date lastUpdated, Date dateCreated, String uniqueId, String name, Enums.LabelType type, Member member, Set<Task> tasks) {
        this.id = id;
        this.lastUpdated = lastUpdated;
        this.dateCreated = dateCreated;
        this.uniqueId = uniqueId;
        this.name = name;
        this.type = type;
        this.member = member;
        this.tasks = tasks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enums.LabelType getType() {
        return type;
    }

    public void setType(Enums.LabelType type) {
        this.type = type;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
}