package com.hackathon.web.command.project;

import com.hackathon.web.model.Project;
import com.hackathon.web.model.user.Member;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by hitenpratap on 28/01/17.
 */
public class ProjectCommand {

    Long id;
    private String name;
    private List<String> memberEmailAddress;
    private Member member;
    String dateCreatedStr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMemberEmailAddress() {
        return memberEmailAddress;
    }

    public void setMemberEmailAddress(List<String> memberEmailAddress) {
        this.memberEmailAddress = memberEmailAddress;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getDateCreatedStr() {
        return dateCreatedStr;
    }

    public void setDateCreatedStr(String dateCreatedStr) {
        this.dateCreatedStr = dateCreatedStr;
    }

    public ProjectCommand() {
    }

    public ProjectCommand(String name, List<String> memberEmailAddress, Member member) {
        this.setName(name);
        this.setMemberEmailAddress(memberEmailAddress);
        this.setMember(member);
    }

    public ProjectCommand(Project project) {
        this.setMember(project.getOwner());
        this.setName(project.getName());
        this.setId(project.getId());
        this.setDateCreatedStr(new SimpleDateFormat("MMM dd, yyyy").format(project.getDateCreated()));
    }
}
