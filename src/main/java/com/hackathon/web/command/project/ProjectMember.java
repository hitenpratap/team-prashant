package com.hackathon.web.command.project;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by hitenpratap on 28/01/17.
 */
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class ProjectMember {

    private List<String> emailAddressList;

    public List<String> getEmailAddressList() {
        return emailAddressList;
    }

    public void setEmailAddressList(List<String> emailAddressList) {
        this.emailAddressList = emailAddressList;
    }
}
