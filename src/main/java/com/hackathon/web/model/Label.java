package com.hackathon.web.model;

import com.hackathon.classes.Enums;
import com.hackathon.web.command.label.LabelCommand;
import com.hackathon.web.model.user.Member;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "label")
public class Label {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated")
    private Date lastUpdated;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "unique_id")
    private String uniqueId = UUID.randomUUID().toString();

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    Enums.LabelType type = Enums.LabelType.MEMBER;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    private Member member;

    @ManyToMany(mappedBy = "labels")
    private Set<Task> tasks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enums.LabelType getType() {
        return type;
    }

    public void setType(Enums.LabelType type) {
        this.type = type;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Label updateLabel(LabelCommand labelCommand) {
        this.name = labelCommand.getName();
        this.type = labelCommand.getType();
        this.member = labelCommand.getMember();
        this.tasks = labelCommand.getTasks();
        return this;
    }

    public Label(LabelCommand labelCommand) {
        this.name = labelCommand.getName();
        this.type = labelCommand.getType();
        this.member = labelCommand.getMember();
    }

    public Label() {
    }
}
