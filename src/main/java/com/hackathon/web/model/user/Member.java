package com.hackathon.web.model.user;

import com.hackathon.web.command.member.MemberCommand;
import com.hackathon.web.model.Comment;
import com.hackathon.web.model.Label;
import com.hackathon.web.model.Project;
import com.hackathon.web.model.Task;
import com.hackathon.web.model.security.User;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "member")
@PrimaryKeyJoinColumn(name = "id")
public class Member extends User {

    @ManyToMany(mappedBy = "members")
    private Set<Project> projects;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
    private Set<Task> tasks;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
    private Set<Comment> comments;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
    private Set<Label> labels;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
    private Set<Project> projectCreatedBy;

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Member(MemberCommand memberCommand) {
        this.setUsername(memberCommand.getUsername());
        this.setFirstName(memberCommand.getFirstName());
        this.setLastName(memberCommand.getLastName());
    }

    public Member() {
    }
}
