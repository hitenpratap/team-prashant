package com.hackathon.web.model.user;

import com.hackathon.web.model.security.User;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by hitenpratap on 28/01/17.
 */
@Entity
@Table(name = "admin")
@PrimaryKeyJoinColumn(name = "id")
public class Admin extends User {

}
