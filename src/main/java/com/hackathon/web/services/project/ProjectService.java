package com.hackathon.web.services.project;

import com.hackathon.web.command.project.ProjectCommand;
import com.hackathon.web.model.Project;
import com.hackathon.web.model.user.Member;
import com.hackathon.web.services.member.MemberService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ProjectService {
    private final Logger logger = LoggerFactory.getLogger(ProjectService.class);

    private SessionFactory sessionFactory;

    @Autowired
    private MemberService memberService;

    @Autowired
    ProjectService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }

    public Long create(ProjectCommand projectCommand) {
        Session session = getSession();
        Set<Member> memberSet = new HashSet<>();
        for (String emailAddress : projectCommand.getMemberEmailAddress()) {
            Member member = memberService.findByUsername(emailAddress);
            memberSet.add(member);
        }
        Project project = new Project(projectCommand, memberSet);
        session.save(project);
        session.flush();
        session.close();
        return project.getId();
    }

    public List<ProjectCommand> list() {
        Session session = getSession();
        List<ProjectCommand> projectCommandList = new ArrayList<>();
        List<Project> projectList = session.createCriteria(Project.class).list();
        for (Project project : projectList) {
            projectCommandList.add(new ProjectCommand(project));
        }
        session.close();
        return projectCommandList;
    }

    public List<ProjectCommand> listCreatedProject() {
        List<ProjectCommand> projectCreatedList = new ArrayList<>();
        return projectCreatedList;
    }

    public ProjectCommand view(Long projectId) {
        Session session = getSession();
        List<Project> projectList = session.createCriteria(Project.class).add(Restrictions.eq("id", projectId)).list();
        Project project = projectList.get(0);
        session.close();
        return new ProjectCommand(project);
    }

}
