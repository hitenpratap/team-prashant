package com.hackathon.web.services.label;

import com.hackathon.classes.Enums;
import com.hackathon.web.command.label.LabelCommand;
import com.hackathon.web.model.Label;
import com.hackathon.web.model.user.Member;
import com.hackathon.web.services.member.MemberService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LabelService {
    private final Logger logger = LoggerFactory.getLogger(LabelService.class);
    private SessionFactory sessionFactory;

    @Autowired
    LabelService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Autowired
    MemberService memberService;

    private Session getSession() {
        return sessionFactory.openSession();
    }

    public List<LabelCommand> list() {
        List<LabelCommand> labelCommands = new ArrayList<>();
        logger.info("////////////////////1//////////////////////");
        logger.info("///////////////////1///////////////////////");
        logger.info("///////////////////1///////////////////////");
        logger.info("///////////////////1///////////////////////");
        logger.info("///////////////////1///////////////////////");
        logger.info("///////////////////1///////////////////////");
        logger.info("///////////////////1///////////////////////");
        Member member = memberService.getCurrentMember();
        System.out.print("member " + member.getId());
        logger.info("///////////////////2///////////////////////");
        logger.info("///////////////////2///////////////////////");
        logger.info("///////////////////2///////////////////////");
        logger.info("///////////////////2///////////////////////");
        Session session = getSession();
        List<Label> labels = session.createCriteria(Label.class).add(Restrictions.eq("member", member)).list();
        logger.info("///////////////////3///////////////////////");
        logger.info("///////////////////3///////////////////////");
        logger.info("///////////////////3///////////////////////");
        logger.info("///////////////////3///////////////////////");
        logger.info("///////////////////3///////////////////////");
        for (Label label : labels) {
            System.out.println(label.getId());
            labelCommands.add(new LabelCommand(label));
        }
        logger.info("///////////////////4///////////////////////");
        logger.info("//////////////////4////////////////////////");
        logger.info("//////////////////4////////////////////////");
        logger.info("///////////////////4///////////////////////");
        logger.info("///////////////////4///////////////////////");
        logger.info("///////////////////4///////////////////////");
        return labelCommands;
    }

    public List<LabelCommand> taskList(Long taskId) {
        List<LabelCommand> labelCommands = new ArrayList<>();
        return labelCommands;
    }

    public Long createLabel(LabelCommand labelCommand) {
        Member member = memberService.getCurrentMember();
        labelCommand.setMember(member);
        labelCommand.setType(Enums.LabelType.MEMBER);
        Label label = new Label(labelCommand);
        getSession().save(label);
        return label.getId();
    }

    public Label getLabel(Long taskId) {
        Session session = getSession();
        Label label = (Label) session.get(Label.class, taskId);
        session.close();
        return label;
    }

    public Label read(Long id) {
        Session session = getSession();
        Label label = (Label) session.get(Label.class, id);
        session.close();
        return label;
    }

    public void update(LabelCommand labelCommand, long labelId) {
        Session session = getSession();
        Member member = memberService.getCurrentMember();
        labelCommand.setMember(member);
        labelCommand.setType(Enums.LabelType.MEMBER);
        Label label = (Label) session.get(Label.class, labelId);
        label = label.updateLabel(labelCommand);
        session.update(label);
        session.flush();
        session.close();
    }

    public void delete(Long id) {
        logger.info("///////////////////////////////////");
        logger.info("///////////////////////////////////");
        logger.info("labelIdlabelIdlabelIdlabelIdlabelIdlabelId");
        logger.info("labelId " + id);
        logger.info("labelIdlabelIdlabelIdlabelIdlabelIdlabelId");
        logger.info("///////////////////////////////////");
        logger.info("///////////////////////////////////");
        logger.info("///////////////////////////////////");
        Session session = getSession();
        Label label = (Label) session.get(Label.class, id);
        session.delete(label);
        logger.info("33333333333333333");
        logger.info("3333333333333");
        logger.info("333333333333333333333333333");
        logger.info("3333333333333333333333333333333333333");
        logger.info("3333333333333333333");
        session.flush();
        session.close();
    }
}