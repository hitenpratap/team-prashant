package com.hackathon.web.services.comment;

import com.hackathon.classes.CommentVO;
import com.hackathon.web.model.Comment;
import com.hackathon.web.model.Label;
import com.hackathon.web.model.Task;
import com.hackathon.web.model.user.Member;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    private final Logger logger = LoggerFactory.getLogger(CommentService.class);
    private SessionFactory sessionFactory;

    @Autowired
    CommentService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }


    public Comment createComment(CommentVO commentVO) {
        Session session = getSession();
        Comment comment = new Comment();
        Task task = (Task) session.get(Task.class, commentVO.taskId);
        Member member = (Member) session.get(Member.class, commentVO.memberId);
        comment.setCommentBody(commentVO.commentBody);
        comment.setTask(task);
        comment.setMember(member);
        session.save(comment);
        session.close();
        return comment;
    }

    public Comment updateComment(CommentVO commentVO) {
        Session session = getSession();
        Comment comment = (Comment) session.get(Comment.class, commentVO.commentId);
        comment.setCommentBody(commentVO.commentBody);
        session.saveOrUpdate(comment);
        session.close();
        return comment;
    }

    public void deleteComment(Integer commentId){
        Session session = getSession();
        Comment comment = (Comment) session.get(Comment.class, commentId);
        session.delete(comment);
    }

    public List<Comment> list(){
        Session session = getSession();
        List<Comment> commentList = session.createCriteria(Comment.class).list();
        return commentList;
    }

}
