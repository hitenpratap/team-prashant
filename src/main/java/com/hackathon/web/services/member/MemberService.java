package com.hackathon.web.services.member;

import com.hackathon.classes.HelperMethod;
import com.hackathon.web.command.member.MemberCommand;
import com.hackathon.web.model.user.Member;
import com.hackathon.web.services.security.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MemberService {

    private final Logger logger = LoggerFactory.getLogger(MemberService.class);

    private SessionFactory sessionFactory;

    @Autowired
    private HelperMethod helperMethod;
    @Autowired
    private UserService userService;

    @Autowired
    MemberService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }


    public List<MemberCommand> list() {
        List<MemberCommand> memberCommands = new ArrayList<>();
        return memberCommands;
    }

    public Member getCurrentMember() {
        Session session = getSession();
        Member member = null;
        String username = helperMethod.getCurrentUserName();
        List<Member> memberList = session.createCriteria(Member.class).add(Restrictions.eq("username", username)).list();
        if ((memberList != null) && memberList.size() > 0) {
            member = memberList.get(0);
        }
        session.close();
        return member;
    }

    public void createDummyMember() {
        for (int i = 1; i <= 10; i++) {
            MemberCommand memberCommand = new MemberCommand();
            memberCommand.setUsername("test" + i + "@email.com");
            memberCommand.setPassword("123456");
            userService.create(memberCommand);
        }
    }

    public Member findByUsername(String emailAddress) {
        Session session = getSession();
        Member member = null;
        List<Member> memberList = session.createCriteria(Member.class).add(Restrictions.eq("username", emailAddress)).list();
        if (memberList != null) {
            member = memberList.get(0);
        }
        session.close();
        return member;
    }
}
