package com.hackathon.web.services.config;

import com.hackathon.web.command.UserCommand;
import com.hackathon.web.model.security.Role;
import com.hackathon.web.model.security.User;
import com.hackathon.web.services.member.MemberService;
import com.hackathon.web.services.security.RoleService;
import com.hackathon.web.services.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
public class BootStrapService implements InitializingBean {

    @Autowired
    UserService userService;

    @Autowired
    MemberService memberService;

    @Autowired
    RoleService roleService;
    private final Logger logger = LoggerFactory.getLogger(BootStrapService.class);

    @Override
    @Transactional
    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        logger.info("********************************************");
        logger.info("Bootstrapping data...");
        if (userService.list().isEmpty()) {
            createAdminUser();
//            createUser();
            memberService.createDummyMember();
        }
        logger.info("********************************************");
    }

    public void createAdminUser() {
        UserCommand adminUserCommand = new UserCommand();
        adminUserCommand.setUsername("admin@admin.com");
        adminUserCommand.setPassword("admin");
        User savedAdmin = userService.read(userService.create(adminUserCommand));
        String admin = "ROLE_ADMIN";
        Role roleAdmin = roleService.read(roleService.create(admin, savedAdmin));
        System.out.println("Admin created with ROLE_ADMIN and username - " + savedAdmin.getUsername() + "  and password -  " + savedAdmin.getPassword());
    }


    public void createUser() {
        UserCommand userCommand = new UserCommand();
        userCommand.setUsername("vijay@nexthoughts.com");
        userCommand.setPassword("123456");
        User savedUser = userService.read(userService.create(userCommand));
        String user = "ROLE_USER";
        Role roleUser = roleService.read(roleService.create(user, savedUser));
        System.out.println("User created with ROLE_USER and username - " + savedUser.getUsername() + "  and password -  " + savedUser.getPassword());

        for (int i = 1; i <= 3; i++) {
            UserCommand userCommand1 = new UserCommand();
            userCommand1.setUsername("test" + i + "@email.com");
            userCommand1.setPassword("123456");
            User savedUser1 = userService.read(userService.create(userCommand1));
            String user1 = "ROLE_USER";
            Role roleUser1 = roleService.read(roleService.create(user1, savedUser1));
            System.out.println("User created with ROLE_USER and username - " + savedUser1.getUsername() + "  and password -  " + savedUser1.getPassword());
        }
    }
}