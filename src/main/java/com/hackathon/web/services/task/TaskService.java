package com.hackathon.web.services.task;

import com.hackathon.classes.TaskVO;
import com.hackathon.web.model.Project;
import com.hackathon.web.model.Task;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {
    private final Logger logger = LoggerFactory.getLogger(TaskService.class);
    private SessionFactory sessionFactory;

    @Autowired
    TaskService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }


    public List<Task>  viewAllTask(TaskVO taskVO) {
        List<Task> taskList =new ArrayList();
        Session session = this.getSession();
        Transaction transaction = session.beginTransaction();

        String hql = "from Project where id=:projectId";
        Query query = session.createQuery(hql);
        query.setParameter("projectId", taskVO.getProjectId());
        List<Project> projectList =query.list();

        if(projectList.size()>0){
            hql = "from Task where project=:project";
            query = session.createQuery(hql);
            query.setParameter("project", projectList.get(0));
            taskList =query.list();
        }

        transaction.commit();
        session.close();
        return taskList;
    }


    public void  createTask(TaskVO taskVO) {
        Session session = this.getSession();
        Transaction transaction = session.beginTransaction();
        String hql = "from Project where id=:projectId";
        Query query = session.createQuery(hql);
        query.setParameter("projectId", taskVO.getProjectId());
        List<Project> projectList =query.list();

        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        if(projectList.size()>0) {
            Task task = new Task();
            task.setDescription(taskVO.getTaskDescription());
            task.setTitle(taskVO.getTaskName());
            task.setProject(projectList.get(0));
            session.save(task);
        }

        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        System.out.println("saxsasdcsdcsdcsd"+projectList.size());
        transaction.commit();
        session.close();
    }

}
