package com.hackathon.web.controllers;

import com.hackathon.classes.Enums;
import com.hackathon.classes.MailAPI;
import com.hackathon.classes.MailVO;
import com.hackathon.web.command.member.MemberCommand;
import com.hackathon.web.model.security.User;
import com.hackathon.web.services.security.RoleService;
import com.hackathon.web.services.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class PublicController {

    private final Logger logger = LoggerFactory.getLogger(PublicController.class);

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @RequestMapping(method = GET, value = "/")
    public String index() {
        logger.info("Request coming at  public/index  ");
        return "public/index";
    }

    @RequestMapping(method = GET, value = "/labels")
    public String label() {
        logger.info("Request coming at  public/labels  ");
        return "label/list";
    }

    @RequestMapping(method = GET, value = "/signup")
    public String signUp(Model model) {
        model.addAttribute(new MemberCommand());
        logger.info("Request coming at  public/signup  ");
        return "signup/signUp";
    }

    @RequestMapping(method = POST, value = "/signup")
    public String processSignUp(MemberCommand memberCommand) {
        logger.info("Request coming at  public/signup  ");
        logger.info("Request coming at  public/signup  ");
        logger.info("Request coming at  public/signup  ");
        logger.info("Request coming at  public/signup  ");
        logger.info("Request coming at  public/signup  ");
        User savedUser = userService.read(userService.create(memberCommand));
        System.out.println("User created with ROLE_USER and username - " + savedUser.getUsername() + "  and password -  " + savedUser.getPassword());
        MailVO mailVO = new MailVO();
        mailVO.setEmailType(Enums.EmailType.SIGN_UP);
        mailVO.setSentToEmail(savedUser.getUsername());
        mailVO.setSentToUserName(savedUser.getFirstName());
        mailVO.setSubject("You have been Registered with Todo");
        mailVO.setDescription("you are registered with todo, now sign in with your credentials to reach space");
        MailAPI.sendMail(mailVO);

        return "signup/signUp";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        logger.debug("==============================");
        List projects = null;
        ModelAndView model = new ModelAndView();
        model.setViewName("project/list");
        model.addObject("projectList", projects);
        return model;
    }
}