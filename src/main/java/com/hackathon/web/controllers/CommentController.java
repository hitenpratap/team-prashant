package com.hackathon.web.controllers;


import com.hackathon.classes.CommentVO;
import com.hackathon.web.command.label.LabelCommand;
import com.hackathon.web.model.Comment;
import com.hackathon.web.model.Label;
import com.hackathon.web.services.comment.CommentService;
import com.hackathon.web.services.label.LabelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/comment")
public class CommentController {

    private final Logger logger = LoggerFactory.getLogger(CommentController.class);
    private CommentService commentService;
    @Autowired
    CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(@PathVariable CommentVO commentVO) {
        Comment comment = commentService.createComment(commentVO);
        ModelAndView model = new ModelAndView();
        model.setViewName("topic/list");
        model.addObject("comment", comment);
        return model;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        logger.debug("==============================");
        List<Comment> commentList = commentService.list();
        ModelAndView model = new ModelAndView();
        model.setViewName("topic/list");
        model.addObject("commentList", commentList);
        return model;
    }

    @RequestMapping(value = "/edit/{commentId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable CommentVO commentVO) {
        ModelAndView modelAndView = new ModelAndView();
        Comment comment = commentService.updateComment(commentVO);
        modelAndView.setViewName("label/edit");
        modelAndView.addObject("comment", comment);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{commentId}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("commentId") int commentId) {
        commentService.deleteComment(commentId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/label/list");
        return modelAndView;
    }

}
