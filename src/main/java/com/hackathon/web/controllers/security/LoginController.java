package com.hackathon.web.controllers.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.unbescape.html.HtmlEscape;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping("/login")
    public String login() {
        logger.info("///////////////GOING TO CALL LOGIN FORM/////////////////////////");
        logger.info("///////////////GOING TO CALL LOGIN FORM/////////////////////////");
        logger.info("///////////////GOING TO CALL LOGIN FORM/////////////////////////");
        logger.info("///////////////GOING TO CALL LOGIN FORM/////////////////////////");
        logger.info("///////////////GOING TO CALL LOGIN FORM/////////////////////////");
        return "login";
    }

    /*@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login() {
        return "index";
    }*/

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        logger.info("EROOOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRR");
        model.addAttribute("loginError", true);
        return "login";
    }


    @RequestMapping("/error")
    public String error(HttpServletRequest request, Model model) {
        model.addAttribute("errorCode", "Error " + request.getAttribute("javax.servlet.error.status_code"));
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("<ul>");
        while (throwable != null) {
            errorMessage.append("<li>").append(HtmlEscape.escapeHtml5(throwable.getMessage())).append("</li>");
            throwable = throwable.getCause();
        }
        errorMessage.append("</ul>");
        model.addAttribute("errorMessage", errorMessage.toString());
        return "error";
    }

    @RequestMapping("/403")
    public String forbidden() {
        return "403";
    }
}
