package com.hackathon.web.controllers.member;

import com.hackathon.web.services.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class MemberController {
    private final Logger logger = LoggerFactory.getLogger(MemberController.class);
    private UserService userService;

    @Autowired
    MemberController(UserService userService) {
        this.userService = userService;
    }


}