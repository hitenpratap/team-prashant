package com.hackathon.web.controllers.label;

import com.hackathon.web.command.label.LabelCommand;
import com.hackathon.web.model.Label;
import com.hackathon.web.services.label.LabelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/label")
public class LabelController {
    private final Logger logger = LoggerFactory.getLogger(LabelController.class);
    private LabelService labelService;

    @Autowired
    LabelController(LabelService labelService) {
        this.labelService = labelService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute(new LabelCommand());
        return "label/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String save(LabelCommand labelCommand) {
        Long labelId = labelService.createLabel(labelCommand);
        logger.info("Label with Id " + labelId + "has been created");
        return "redirect:/label/list";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        logger.debug("==============================");
        List<LabelCommand> labelCommandList = labelService.list();
        ModelAndView model = new ModelAndView();
        model.setViewName("label/list");
        model.addObject("labelCommandList", labelCommandList);
        return model;
    }

    @RequestMapping(value = "/edit/{labelId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("labelId") Long labelId) {
        ModelAndView modelAndView = new ModelAndView();
        Label label = labelService.read(labelId);
        modelAndView.setViewName("label/edit");
        modelAndView.addObject("label", label);
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{labelId}", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable("labelId") int labelId, LabelCommand labelCommand) {
        labelService.update(labelCommand, labelId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/label/list");
        modelAndView.addObject("labelCommand", labelCommand);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{labelId}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("labelId") Long labelId) {
        labelService.delete(labelId);
        ModelAndView modelAndView = new ModelAndView();
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        logger.info("/////////////////////////////////???????????????????????????????");
        modelAndView.setViewName("redirect:/label/list");
        return modelAndView;
    }
}