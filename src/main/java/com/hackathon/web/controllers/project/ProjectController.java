package com.hackathon.web.controllers.project;

import com.hackathon.classes.Enums;
import com.hackathon.classes.MailAPI;
import com.hackathon.classes.MailVO;
import com.hackathon.classes.ResultVO;
import com.hackathon.web.command.project.ProjectCommand;
import com.hackathon.web.command.project.ProjectMember;
import com.hackathon.web.model.user.Member;
import com.hackathon.web.services.member.MemberService;
import com.hackathon.web.services.project.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by hitenpratap on 28/01/17.
 */
@Controller
@RequestMapping(value = "/project")
public class ProjectController {

    final private Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ProjectMember projectMember;

    @Secured({"ROLE_USER"})
    @RequestMapping(method = GET, value = "/create")
    public String create() {
        return "project/create";
    }

    @Secured({"ROLE_USER"})
    @RequestMapping(method = POST, value = "/create")
    public String save(@RequestParam String name) {
        List<String> memberEmailAddress = projectMember.getEmailAddressList();
        for(String emailId:memberEmailAddress){
            MailVO mailVO=new MailVO();
            mailVO.setEmailType(Enums.EmailType.NEW_USER_ADDED_TO_PROJECT) ;
            mailVO.setSentToEmail(emailId);
            mailVO.setSentToUserName("prasahant");
            mailVO.setProjectName("fin360");
            mailVO.setSubject("Yoh have been Added to the ");
            mailVO.setSentByUserName("agarwal");
            mailVO.setDescription("agarwal");
            MailAPI.sendMail(mailVO);
        }
        projectMember.setEmailAddressList(null);
        Member member = memberService.getCurrentMember();
        ProjectCommand projectCommand = new ProjectCommand(name, memberEmailAddress, member);
        Long id = projectService.create(projectCommand);
        return "redirect:/project/list";
    }

    @Secured({"ROLE_USER"})
    @RequestMapping(method = POST, value = "/addSubscriber")
    @ResponseBody
    public String addSubscriber(@RequestParam String emailAddress) {
        Enums.Result result = Enums.Result.SUCCESS;
        List<String> emailAddressList = new ArrayList<>();
        if (projectMember.getEmailAddressList() != null) {
            emailAddressList = projectMember.getEmailAddressList();
        }
        emailAddressList.add(emailAddress);
        projectMember.setEmailAddressList(emailAddressList);
        return result.name();
    }

    @Secured({"ROLE_USER"})
    @RequestMapping(method = GET, value = "/list")
    public String list(Model model) {
        List<ProjectCommand> projectList = projectService.list();
        model.addAttribute("projectList", projectList);
        return "project/list";
    }

}
