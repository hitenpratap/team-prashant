package com.hackathon.web.controllers;

import com.hackathon.classes.TaskVO;
import com.hackathon.web.command.project.ProjectCommand;
import com.hackathon.web.model.Task;
import com.hackathon.web.services.project.ProjectService;
import com.hackathon.web.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping({"/task"})
public class TaskController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TaskService taskService;

    @RequestMapping(method = GET, value = "/open")
    public String openPage() {
        return "task/createTask";
    }
    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public String createTask(WebRequest request){
        String taskName = request.getParameter("taskName");
        String taskDescription = request.getParameter("taskDescription");
        TaskVO taskVO=new TaskVO();
        taskVO.setTaskName(taskName);
        taskVO.setProjectId(Long.valueOf(1));
        taskVO.setTaskDescription(taskDescription);
        taskService.createTask(taskVO);
        return "task/createTask";
    }

    @RequestMapping(method = GET, value = "/list/{projectId}")
    public ModelAndView ViewAllTask(@PathVariable Long projectId) {
        ProjectCommand projectCommand = projectService.view(projectId);
        TaskVO taskVO = new TaskVO();
        taskVO.setProjectId(projectId);
        List<Task> taskList = taskService.viewAllTask(taskVO);
        ModelAndView model = new ModelAndView();
        model.setViewName("task/task");
        model.addObject("create", taskList);
        model.addObject("projectCommand", projectCommand);
        return model;
    }
}
