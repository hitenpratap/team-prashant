package com.hackathon.web.controllers;

import com.hackathon.classes.Enums;
import com.hackathon.classes.MailAPI;
import com.hackathon.classes.MailVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping({"/mailTest"})
public class MailTestController {
    @RequestMapping(method = GET, value = "/test")
    public String index() {
        MailVO mailVO=new MailVO();
        mailVO.setEmailType(Enums.EmailType.TASK_ASSIGNED) ;
        if(mailVO.getEmailType() == Enums.EmailType.SIGN_UP)
        {
            mailVO.setSentToEmail("prashant1992agarwal@gmail.com");
            mailVO.setSentToUserName("prashant");
            mailVO.setSubject("prashant");
            mailVO.setDescription("you have been subscribed");
        }
        else if(mailVO.getEmailType() == Enums.EmailType.TASK_ASSIGNED){
            mailVO.setSentToEmail("prashant1992agarwal@gmail.com");
            mailVO.setSentToUserName("prashant");
            mailVO.setSubject("prashant");

            mailVO.setSentByUserName("prashant agarwal");
            mailVO.setDescription("task have been asigned");

        }
        else if(mailVO.getEmailType() == Enums.EmailType.NEW_USER_ADDED_TO_PROJECT){

            mailVO.setSentToEmail("prashant1992agarwal@gmail.com");
            mailVO.setSentToUserName("prasahant");
            mailVO.setProjectName("fin360");
            mailVO.setSubject("prashant");
            mailVO.setSentByUserName("agarwal");
            mailVO.setDescription("agarwal");

        }

        MailAPI.sendMail(mailVO);
        return "public/index";
    }

}
